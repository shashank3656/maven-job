import gitlab
import os
#PRIVATE_TOKEN = 'czkH5_hFVAB92etnyXyf'
gl = gitlab.Gitlab(os.environ['CI_SERVER_URL'], private_token=os.environ['PRIVATE_TOKEN'])
project = gl.projects.get(os.environ['CI_PROJECT_ID'])

issue_details = {
'title': f'Validation failed in {os.environ["CI_PROJECT_NAME"]}',
'description': f'Pipeline: {os.environ["CI_PIPELINE_URL"]}',
'job_id': f'Job: {os.environ["CI_JOB_ID"]}'
}
issue = project.issues.create(issue_details )
